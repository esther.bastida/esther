from functions import libration_point_location, libration_point_location_CoM, plot_libration_points

def test_libration_point_location_123(point_number, mu):

    '''
    Testing the libration_point_location_Halo original function for L1, L2, L3.

    References:
        - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. 
        Chapter 4. Page 139. Figure 4.5. 
        - GMATMathSpec.
    Results checked by knowing real Lagrangian points' positions in the Sun-Earth system.

    Parameters
    ----------
    point_number : integer
        Id of lagrange point
    mu : float
        m2 / (m1 + m2)

    Returns
    -------
    gammax : float
        x-distance of the libration point to the nearest mass (adimensional)
    gammay : float
        y-distance of the libration point to the nearest mass (adimensional)
    '''

    gammax, gammay = libration_point_location(point_number, mu)
    print("gammax = ", gammax, "; gammay = ", gammay)

'''
Running the test, uncomment following line
'''
# test_libration_point_location_Halo_123(3, 3.0034 * 10**-6) # For the Earth-Sun system

def test_libration_point_location_45(point_number, mu):

    '''
    Testing the libration_point_location_Halo function for L4, L5.

    References:
        - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. 
        Chapter 4. Page 139. Figure 4.5. 
        - GMATMathSpec.
    
    Parameters
    ----------
    point_number : integer
        Id of lagrange point
    mu : float
        m2 / (m1 + m2)

    Returns
    -------
    gammax : float
        x-distance of the libration point to the nearest mass (adimensional)
    gammay : float
        y-distance of the libration point to the nearest mass (adimensional)
    '''

    gammax, gammay = libration_point_location(point_number, mu)
    print("gammax = ", gammax, "; gammay = ", gammay)

'''
Running the test, uncomment following line
'''
# test_libration_point_location_Halo_45(4, 3.0034 * 10**-6) # For the Earth-Sun system

def test_libration_point_location_CoM(point_number, mu):

    '''
    Testing the libration_point_location_CoM_Halo function.

    References:
        - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. 
        Chapter 4. Page 139. Figure 4.5. 
        - GMATMathSpec.

    Parameters
    ----------
    point_number : integer
        Id of lagrange point
    mu : float
        m2 / (m1 + m2)

    Returns
    -------
    x : float
        x-position of the libration point to the center of mass (adimensional)
    y : float
        y-position of the libration point to the center of mass (adimensional)
    '''

    x, y = libration_point_location_CoM(point_number, mu)
    print("x = ", x, "; y = ", y)

'''
Running the test, uncomment following line
'''
# test_libration_point_location_CoM_Halo(5, 3.0034 * 10**-6) # For the Earth-Sun system

def test_plot_libration_points(mu):
    """
    Test function that plots the primaries, their orbits and the five libration points' locations.

    References: 
    - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. Chapter 4.
    - GMATMathSpec
    
    Parameters
    ----------
    mu : float
        m2 / (m1 + m2)
    """ 
    plot_libration_points(mu) 

'''
Running the test, uncomment following line
'''
test_plot_libration_points(0.3) # mu that allows clear separation between libration points
