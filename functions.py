from math import sqrt
from scipy.optimize import newton
import matplotlib.pyplot as plt

def libration_point_location(point_number, mu):
    """
    Returns adimensional position of colinear libration points to the nearest primary.

    References: 
    - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. Chapter 4.
    - GMATMathSpec
    
    Parameters
    ----------
    mu : float
        m2 / (m1 + m2)
    point_number : integer
        Id of lagrange point

    Returns
    -------
    gammax : float
        x-distance of the libration point to the nearest mass (adimensional)
    gammay : float
        y-distance of the libration point to the nearest mass (adimensional)
    """
    
    p5 = 1
    
    if point_number == 1:
        p4 = - (3 - mu)
        p3 = 3 - 2 * mu
        p2 = - mu
        p1 = 2 * mu
        p0 = - mu

        x0 = (mu / (3 * (1 - mu)) )**(1. / 3) # initial guess for the iterative procedure

    elif point_number == 2:
        p4 = 3 - mu
        p3 = 3 - 2 * mu
        p2 = - mu
        p1 = - 2 * mu
        p0 = - mu

        x0 = (mu / (3 * (1 - mu)) )**(1. / 3) # initial guess for the iterative procedure

    elif point_number == 3:
        p4 = 2 + mu
        p3 = 1 + 2 * mu
        p2 = - (1 - mu)
        p1 = - 2 * (1 - mu)
        p0 = - (1 - mu)

        x0 = 1 - 7 / 12.0 * mu                # initial guess for the iterative procedure (why not 1?)

    def f(x, p0, p1, p2, p3, p4, p5):
        return p5 * x**5 + p4 * x**4 + p3 * x**3 + p2 * x**2 + p1 * x + p0

    if point_number == 1 or point_number == 2 or point_number == 3:
        gammax = newton(f,x0=x0, args=(p0, p1, p2, p3, p4, p5,), maxiter=10000)
        gammay = 0

    elif point_number == 4 or point_number == 5: 
        gammax = 1 / 2
        gammay = sqrt(3) / 2  

    else:
        print("Invalid point number") 
    
    return gammax, gammay

def libration_point_location_CoM(point_number, mu):
    """
    Returns adimensional position of colinear libration points to the center of mass of the primaries system.

    References: 
    - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. Chapter 4.
    - GMATMathSpec
    
    Parameters
    ----------
    mu : float
        m2 / (m1 + m2)
    point_number : integer
        Id of lagrange point

    Returns
    -------
    x : float
        x-position of the libration point to the center of mass (adimensional)
    y : float
        y-position of the libration point to the center of mass (adimensional)
    """ 

    gammax, gammay = libration_point_location(point_number, mu)

    if point_number == 1:
        x = 1 - mu - gammax
        y = gammay # 0
    
    elif point_number == 2:
        x = 1 - mu + gammax
        y = gammay # 0

    elif point_number == 3:
        x = - (mu + gammax)
        y = gammay # 0

    elif point_number == 4:
        x = gammax - mu 
        y = gammay

    elif point_number == 5:
        x = gammax - mu 
        y = - gammay

    return x, y

def plot_libration_points(mu):
    """
    Returns plot including the primaries, their orbits and the five libration points' locations.

    References: 
    - V. G. Szebehely, Theory of orbits, the restricted problem of three bodies. Chapter 4.
    - GMATMathSpec
    
    Parameters
    ----------
    mu : float
        m2 / (m1 + m2)
    """ 

    # Obtaining libration points' coordinates with respect to center of mass
    x1, y1 = libration_point_location_CoM(1, mu)
    x2, y2 = libration_point_location_CoM(2, mu)
    x3, y3 = libration_point_location_CoM(3, mu)
    x4, y4 = libration_point_location_CoM(4, mu)
    x5, y5 = libration_point_location_CoM(5, mu)

    # Plotting primaries and their orbits
    primary1 = plt.Circle((- mu, 0), 0.1, color='black')
    primary2 = plt.Circle((1 - mu, 0), 0.05, color='black')
    orbit_2 = plt.Circle((- mu, 0), 1, color='blue', fill=False)
    _, ax = plt.subplots() 
    ax.add_patch(primary1)
    ax.add_patch(primary2)
    ax.add_patch(orbit_2)

    # Plotting libration points
    ax.plot(x1, y1, 'x', color='black', lw=1)
    plt.annotate("L1", (x1 + 0.05, y1 + 0.05), textcoords="offset points", xytext=(0, 0))
    ax.plot(x2, y2, 'x', color='black', lw=1)
    plt.annotate("L2", (x2 + 0.05, y2 + 0.05), textcoords="offset points", xytext=(0, 0))
    ax.plot(x3, y3, 'x', color='black', lw=1)
    plt.annotate("L3", (x3 + 0.05, y3 + 0.05), textcoords="offset points", xytext=(0, 0))
    ax.plot(x4, y4, 'x', color='black', lw=1)
    plt.annotate("L4", (x4 + 0.05, y4 + 0.05), textcoords="offset points", xytext=(0, 0))
    ax.plot(x5, y5, 'x', color='black', lw=1)
    plt.annotate("L5", (x5 + 0.05, y5 + 0.05), textcoords="offset points", xytext=(0, 0))

    # Plot general properties
    plt.axis([-2, 2, -2, 2])
    ax.set_aspect(1)
    plt.show()
